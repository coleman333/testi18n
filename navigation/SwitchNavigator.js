import React from 'react';
import { createSwitchNavigator} from 'react-navigation';
import {ApplicationNavigator} from "./drawerNavigator";

const SwitchNavigator = createSwitchNavigator({
    // MainMenu: MainMenu,
    // Navigator: Navigator,
    // drawerNavigator: drawerNavigator
    ApplicationNavigator: ApplicationNavigator
    },{
    navigationOptions: {
        header: null,
    },
    }
);

export default SwitchNavigator;
