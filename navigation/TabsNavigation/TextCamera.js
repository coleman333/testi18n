import React from 'react';
import { createStackNavigator } from 'react-navigation';

import  TextCamera  from '../../containers/TextCamera/textCamera';
// import { Header } from '../header';
import { TabIcon } from '../tab-icon';

export const TextCameraTab = {
    screen: createStackNavigator(
        {
            TextCamera: TextCamera,
        },
        {
            navigationOptions: {
                header: null,
            },
        },
    ),
    navigationOptions: {
        tabBarLabel: 'scan text',
        tabBarIcon: ({ focused } : {focused: boolean}) => <TabIcon tab="scan" focused = {focused} />
    },
};
