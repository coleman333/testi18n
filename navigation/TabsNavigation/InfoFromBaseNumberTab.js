import React from 'react';
import { createStackNavigator } from 'react-navigation';

import  InfoFromBaseByNumber  from '../../containers/InfoFromBaseByNumber/InfoFromBaseByNumber';
// import { Header } from '../header';
import { TabIcon } from '../tab-icon';

export const InfoFromBaseByNumberTab = {
    screen: createStackNavigator(
        {
            InfoFromBaseByNumber: InfoFromBaseByNumber,
        },
        {
            // initialRouteName: MainMenu,
            // navigationOptions: {
            //     header: <Header theme="light" align="right" />,
            // },
        },
    ),
    navigationOptions: {
        tabBarLabel: 'Write',
        tabBarIcon: ({ focused } : {focused: boolean}) => <TabIcon tab="rewards" focused = {focused} />
    },
};
