import React from 'react';
import { createStackNavigator } from 'react-navigation';

import  BarCodeScanner  from '../../containers/BarCodeScanner/BarCodeScanner';
// import { Header } from '../header';
import { TabIcon } from '../tab-icon';

export const BarCodeScannerTab = {
    screen: createStackNavigator(
        {
            BarCodeScanner: BarCodeScanner,
        },
        {
            navigationOptions: {
                header: null,
            },
        },
    ),
    navigationOptions: {
        tabBarLabel: 'qr scan',
        tabBarIcon: ({ focused } : {focused: boolean}) => <TabIcon tab="wallet" focused = {focused} />
    },
};
