import React from 'react';
import { createStackNavigator } from 'react-navigation';

import  ItemInfoFromBase  from '../../containers/ItemInfoFromBase/ItemInfoFromBase';
// import { Header } from '../header';
import { TabIcon } from '../tab-icon';

export const ItemInfoFromBaseTab = {
    screen: createStackNavigator(
        {
            ItemInfoFromBase: ItemInfoFromBase,
        },
        {
            navigationOptions: {
                header: null,
            },
        },
    ),
    navigationOptions: {
        tabBarLabel: 'list',
        tabBarIcon: ({ focused } : {focused: boolean}) => <TabIcon tab="logout" focused = {focused} />
    },
};
