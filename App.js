// import React, {Component} from 'react';
// import {Platform, StyleSheet, Text, View,Button} from 'react-native';
// import './i18n';
// import { withTranslation } from 'react-i18next';
//
// type Props = {};
// class App extends Component<Props> {
//   changeLang(lng){
//     this.props.i18n.changeLanguage(lng)
//
//   }
//   render() {
//     const { t } = this.props;
//
//     return (
//       <View style={styles.container}>
//         <Text style={styles.welcome}>{ t('theme') }</Text>
//         <Button title={'ru'} onPress={this.changeLang.bind(this,'ru')}/>
//         <Button title={'en'} onPress={this.changeLang.bind(this,'en')}/>
//       </View>
//     );
//   }
// }
//
// export default withTranslation()(App)
//
// const styles = StyleSheet.create({
//   container: {
//     flex: 1,
//     justifyContent: 'center',
//     alignItems: 'center',
//     backgroundColor: '#F5FCFF',
//   },
//   welcome: {
//     fontSize: 20,
//     textAlign: 'center',
//     margin: 10,
//   },
//   instructions: {
//     textAlign: 'center',
//     color: '#333333',
//     marginBottom: 5,
//   },
// });


import React, {Component} from 'react';
// import {AsyncStorage, Platform, StyleSheet, Button, Text, View,Alert} from 'react-native';
// import { Platform, StyleSheet, Button, Text, View} from 'react-native';
const store = configureStore();
import {connect, Provider} from "react-redux";
import configureStore from './configureStore';
import Navigator from "./navigation/Navigator";
import DrawerNavigator from "./navigation/drawerNavigator";
import SwitchNavigator from "./navigation/SwitchNavigator";
import userActions from './actions/userAction';

import './i18n';
// import { withTranslation } from 'react-i18next';


// import i18next from 'i18next';
// import { I18nextProvider } from 'react-i18next';
// import './i18n';
// import { translate, Trans } from 'react-i18next';


import { AsyncStorage,Alert } from 'react-native';
import {ThemeContext} from './Constants/Theme'
// import firebase from 'react-native-firebase'
import {changeTheme, changeLang} from './actions/actions';
// import GestureRecognizer, {swipeDirections} from 'react-native-swipe-gestures';
// import { settings } from "./images/settings.png";
global.Symbol = require('core-js/es6/symbol');
require('core-js/fn/symbol/iterator');
require('core-js/fn/map');
require('core-js/fn/set');
require('core-js/fn/array/find');

import { YellowBox } from 'react-native';
import _ from 'lodash';

// import React from 'react'
import { Text, View, Dimensions, Image, Animated ,Button} from 'react-native'

import {settings} from "./images/settings.png";
import MainMenu from "./containers/MainMenu/MainMenu";
import {bindActionCreators} from "redux";

// const firebaseDB = require("firebase");

// require("firebase-messaging");

// // Required for side-effects
// require("firebase/firestore");

const { height } = Dimensions.get('window');


// Initialize Firebase
// var config = {
//   apiKey: "AIzaSyD0Wux8-CAn5a3qcS8cpxOTO6GDRLjKYSg",
//   authDomain: "test-8fbaf.firebaseapp.com",
//   databaseURL: "https://test-8fbaf.firebaseio.com",
//   projectId: "test-8fbaf",
//   storageBucket: "test-8fbaf.appspot.com",
//   messagingSenderId: "17065363702"
// };

// Initialize Cloud Firestore through Firebase
// firebaseDB.initializeApp(config);

// var db = firebaseDB.database();

type Props = {};

YellowBox.ignoreWarnings(['Setting a timer']);
const _console = _.clone(console);
console.warn = message => {
  if (message.indexOf('Setting a timer') <= -1) {
    _console.warn(message);
  }
};

class App extends Component<Props> {

  fcmToken = {};

  // changeLang(lng){
  //   this.props.i18n.changeLanguage(lng)
  //
  // }

  async componentDidMount(){
    // try {
    //     await AsyncStorage.setItem('port', '3003');
    // } catch (error) {
    //     console.log(error);
    // }
    // try {
    //     await  AsyncStorage.setItem('address', '192.168.0.201');
    // } catch (error) {
    //     console.log(error);
    // }
    // this.onTokenRefreshListener = firebase.messaging().onTokenRefresh(fcmToken => {
    // Process your token as required
    // });

    // this.checkPermission();
    // this.createNotificationListeners(); //add this line

  }

  async componentWillMount(){
    // const theme = await AsyncStorage.getItem('THEME');
    // const lng = (await AsyncStorage.getItem('lng')) || this.props.lng;
    // await i18next.changeLanguage(lng);
    // this.props.changeLang(lng);
    // return this.props.changeTheme(theme);
  }

  componentWillUnmount() {
    // this.onTokenRefreshListener();


    // this.notificationListener();
    // this.notificationOpenedListener();
  }

  static defaultProps = {
    draggableRange: {
      top: height / 1,
      bottom: height * 0.8,
    },
  };

  async checkPermission() {
    const enabled = await firebase.messaging().hasPermission();
    if (enabled) {
      console.log('=============',enabled);
      this.getToken();

    } else {
      this.requestPermission();
    }
  }

  async getToken() {
    let fcmToken = await AsyncStorage.getItem('fcmToken');
    console.log('=============++++++++++',fcmToken);
    if (!fcmToken) {
      fcmToken = await firebase.messaging().getToken();
      console.log('=============++++++++++',fcmToken);
      if (fcmToken) {
        // user has a device token

        await AsyncStorage.setItem('fcmToken', fcmToken);
        console.log('=============++++++++++',fcmToken);

      }
    }
  }

  async requestPermission() {
    try {
      await firebase.messaging().requestPermission();
      // User has authorised
      this.getToken();
    } catch (error) {
      // User has rejected permissions
      console.log('permission rejected');
    }
  }

  async createNotificationListeners() {
    /*
    * Triggered when a particular notification has been received in foreground
    * */
    this.notificationListener = firebase.notifications().onNotification((notification) => {
      const { title, body } = notification;
      this.showAlert(title, body);
    });

    /*
    * If your app is in background, you can listen for when a notification is clicked / tapped / opened as follows:
    * */
    this.notificationOpenedListener = firebase.notifications().onNotificationOpened((notificationOpen) => {
      const { title, body } = notificationOpen.notification;
      this.showAlert(title, body);
    });

    /*
    * If your app is closed, you can check if it was opened by a notification being clicked / tapped / opened as follows:
    * */
    const notificationOpen = await firebase.notifications().getInitialNotification();
    if (notificationOpen) {
      const { title, body } = notificationOpen.notification;
      this.showAlert(title, body);
    }
    /*
    * Triggered for data only payload in foreground
    * */
    this.messageListener = firebase.messaging().onMessage((message) => {
      //process data message
      console.log(JSON.stringify(message));
    });
  }

  showAlert(title, body) {
    Alert.alert(
        title, body,
        [
          { text: 'OK', onPress: () => console.log('OK Pressed') },
        ],
        { cancelable: false },
    );
  }


  sendMessage(){
    // const message = new firebase.messaging.RemoteMessage()
    //     .setMessageId('eaz_OGBsTos:APA91bHnykHbLbJaSHoDY5EnJ_CjmEwPpizqe9kZOlA4RiECE_0-JOm4qpIA19N7DOYZ9j6rhtqOVtA5nts5cyEoBE25nhMNxMPECj9ZIXQgXVeMZLNZQh1Zs1F5ExEZ3gVEEcHJw14N')
    //     .setTo('eaz_OGBsTos:APA91bHnykHbLbJaSHoDY5EnJ_CjmEwPpizqe9kZOlA4RiECE_0-JOm4qpIA19N7DOYZ9j6rhtqOVtA5nts5cyEoBE25nhMNxMPECj9ZIXQgXVeMZLNZQh1Zs1F5ExEZ3gVEEcHJw14N@gcm.googleapis.com')
    //     .setData({
    //       key1: 'value1',
    //       key2: 'value2',
    //     });
    // // Send the message
    // firebase.messaging().sendMessage(message);
  }

  async getDeviceToken(){
    // const fcmToken = await firebase.messaging().getToken();
    // console.log(fcmToken)
    // if (fcmToken) {
    //   // user has a device token
    //   console.log(fcmToken)
    // } else {
    //   // user doesn't have a device token yet
    //   console.log(fcmToken)

    // }
  }

  getPushNotification(){

  }

  sendPicture(){
    // // const SettingsIcon = settings;
    // let SettingsIcon = 'example string';
    //
    // // this.props.userActions.sendPictureToFirebase()
    //
    // fetch(`https://test-8fbaf.firebaseio.com/icons.json`,{
    //   method: "POST",
    //   body: JSON.stringify(SettingsIcon)
    // })
    //     .catch(err=>error.log(err))
    //     .then(res=>res.json())
    //     .then(parsedRes =>{
    //       console.log(parsedRes);
    //     } )
  }

  writeSimpleCode() {
    var user = {
      id:1,
      first: "Ada",
      last: "Lovelace",
      born: 1815
    };
    console.log('+++++++++++++++++++++++++++', db);
    var ref = db.ref('/users/'+user.id).push(user)
    // .then(function () {
    //     console.log("Document written with ID: ");
    // })
    // .catch(function (error) {
    //     console.error("Error adding document: ", error);
    // });
    console.log('+++++++++++++++++++++',ref.key);
  }
  // readFirebaseOnce() {
  //     console.log('++++++++++++++++++++++++++');
  //     // var userId = firebase.auth().currentUser.uid;
  //     let userId = 1;
  //     return db.ref('/users/' + userId).once('value')
  //         .then(function(snapshot) {
  //             var val = snapshot.val();
  //         var username = val  && (val.first+val.last) || 'Anonymous';
  //         console.log('username',username)
  //     });
  // }

  readFirebaseConst(){
    var constRead = db.ref('/users/' + 1 );
    constRead.on('value', function(snapshot) {
      console.log('updateStarCount=>>',snapshot&&snapshot.val())
    });
  }

  deleteFirebaseConst(){
    var deleteCollection = db.ref('/users/' +1);
    deleteCollection.remove();
    // var updates = {};
    // updates['/users/' + 1+'/roles/1'] = null;
    //
    // return db.ref().update(updates);
  }

  //insert date and file

  updateFirebase(uid, username, picture, title, body){
    // A post entry.
    var user = {
      id:1,
      // first: "baraka11",
      // last: "Lovelace",
      // born: 1815,
      roles:[{
        id:1,
        name:'admin',
        userUniquePass: 'secret'
      },{
        id:2,
        name:'user',
        userUniquePass: 'user-secret2'
      }]
    };

    // Get a key for a new Post.
    var newPostKey = db.ref().child('posts').push().key;

    // Write the new post's data simultaneously in the posts list and the user's post list.
    var updates = {};
    updates['/users/' + user.id+'/roles/0'] = {name:'admin',userUniquePass:'super-secret21'};

    return db.ref().update(updates);

  }

  _draggedValue = new Animated.Value( height * 0.8 );

  render() {

    const { top, bottom } = this.props.draggableRange;

    const draggedValue = this._draggedValue.interpolate({
      inputRange: [bottom, top],
      outputRange: [0, 1],
      extrapolate: 'clamp',
    });
    const transform = [{ scale: draggedValue }];
    const {theme}=this.props;
    return (
        <Provider store={store}>
          {/*<I18nextProvider i18n = { i18next } >*/}
            <ThemeContext.Provider value={theme}>
              <SwitchNavigator/>
              {/*<MainMenu/>*/}
            </ThemeContext.Provider>
          {/*</I18nextProvider>*/}
        </Provider>
    );

  }
}


const mapStateToProps = (state) => {
  return {
    theme: state.common.theme,
    // lng: state.common.lng,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    // changeLang: bindActionCreators(changeLang, dispatch),
    changeTheme: bindActionCreators(changeTheme, dispatch)
  }
};

function connectWithStore(store, WrappedComponent, ...args) {
  let ConnectedWrappedComponent = connect(...args)(WrappedComponent)
  return function (props) {
    return <ConnectedWrappedComponent {...props} store={store} />
  }
}

export default connectWithStore(store, App, mapStateToProps, mapDispatchToProps);


const styles = {
  container: {
    flex: 1,
    backgroundColor: '#f8f9fa',
    alignItems: 'center',
    justifyContent: 'center',
    // borderTop: 'solid black 2px'
    borderTopColor: 'black',
    borderTopWidth: 1,
  },
  panel: {
    flex: 1,
    backgroundColor: 'white',
    position: 'relative',
  },
  panelHeader: {
    height: 120,
    backgroundColor: '#b197fc',
    alignItems: 'center',
    justifyContent: 'center',
  },
  favoriteIcon: {
    position: 'absolute',
    top: -24,
    right: 24,
    backgroundColor: '#2b8a3e',
    width: 48,
    height: 48,
    padding: 8,
    borderRadius: 24,
    zIndex: 1,
  },
}


