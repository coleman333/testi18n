import React from 'react';
export const GRAY = {
    Background: 'gray',
    SeparatorColor: 'black',
    FirstSeparatorColor: 'black',
    TextStyle: 'black',
    ButtonColor: 'black',
    color: 'black',
    fontSize: 18
};

export const DARK = {
    Background: '#101010',
    SeparatorColor: 'white',
    FirstSeparatorColor: 'white',
    TextStyle: 'white',
    ButtonColor: 'white',
    color: 'white',
    fontSize: 18
};
export const ThemeContext = React.createContext();