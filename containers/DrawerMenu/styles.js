import {StyleSheet} from "react-native";
import * as themes from "../../Constants/Theme";

export const liStyle = (color) => StyleSheet.create({
    mainContainer: {
        flex: 1,
        // backgroundColor: GRAY.DrawerBackground
        // backgroundColor: 'gray'
        backgroundColor: themes[color].Background
    },
    menuItem: {
        width: "100%",
        height: 60,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
        // elevation: 8
    },
    iconStyle: {
        height: 40,
        width: 40,
    },
    imageContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        width: '30%',
        height: 60,
    },
    textContainer: {
        justifyContent: 'center',
        // alignItems: 'center',
        width: '70%',
        height: 60,
    },
    separator: {
        width: '90%',
        height: 2,
        borderBottomWidth: 1,
        borderBottomColor: themes[color].SeparatorColor,
        marginTop: 2,
        marginBottom: 2,
        marginLeft: "5%",
        // marginRight:5
    },
    firstSeparator: {
        width: '90%',
        height: 2,
        borderBottomWidth: 1,
        borderBottomColor: themes[color].FirstSeparatorColor,
        marginTop: 100,
        marginBottom: 2,
        marginLeft: "5%",
    },
    textStyle: {
        color: themes[color].TextStyle,
        fontSize: 20,
        marginLeft: 10
    }
})
