import { StyleSheet } from 'react-native';
import * as themes from '../../Constants/Theme';

export const styles = (color)=> StyleSheet.create({
    MainContainer:{
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: themes[color].Background,
        height: "100%"
    },
    InnerMainContainer: {
        flex: 8,
        width: '100%',
        justifyContent: 'center',
        alignItems: 'center'
    },
    ContainerLabel: {
        marginTop: 10,
        // height: 50,
        width: '100%',
        // backgroundColor: '#eee',
        // borderWidth:1,
        // borderRadius:25,
        justifyContent: 'center',
    },
    scrollViewContainer: {
        width: '90%',
    },

    buttonStyle:{
        borderWidth: 1,
        // borderColor:'rgba(0,0,0,0.2)',
        borderColor: themes[color].ButtonColor,
        alignItems: 'center',
        justifyContent: 'center',
        width: 200,
        height: 50,
        backgroundColor: themes[color].Background,
        borderRadius: 50,
    },
    textLabel: {
        textAlign: 'center',
        alignItems: 'center',
        color: themes[color].color
    },
    switchContainer:{
        alignItems: 'center',
        flexDirection: 'row',
        marginTop: 15,
        justifyContent: 'space-between'
    },
    innerSwitchViewContainer:{
        justifyContent: 'flex-end'
    },
    switchTextLabel:{
      color: themes[color].color
    },
    textInput: {
        borderColor: themes[color].ButtonColor,
        marginTop: 10,
        height: 50,
        width: '100%',
        backgroundColor: '#eee',
        borderWidth: 1,
        borderRadius: 25,
        justifyContent: 'center',
        textAlign: "center"
    }
})
