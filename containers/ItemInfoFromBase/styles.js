import {StyleSheet} from "react-native";
import * as themes from '../../Constants/Theme';

export const styles = (color) => StyleSheet.create({
    MainContainer:{
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: themes[color].Background,
        height: "100%"
    },
    MainInnerContainer: {
        flex:8,
        width:'98%',
        justifyContent:'center',
        alignItems:'center',
        borderWidth:1,
        borderColor: themes[color].color,
        borderRadius:10,
        marginBottom:10,
        marginTop:5,
        padding:10,
        color: themes[color].color,
    },
    ActivityIndicatorContainer: {
        height:'100%',
        width:'100%',
        justifyContent: 'center',
        position:'absolute',
        alignItems: 'center',
        backgroundColor: themes[color].Background,
    },
    ContainerLabel:{
        marginTop:10,
        // height: 50,
        width:'100%',
        justifyContent: 'flex-start',
    },
    ContainerLabelRight:{
        marginTop:10,
        // height: 50,
        width:'100%',
        justifyContent: 'flex-end',
    },
    textLabel:{
        textAlign:'left',
        // alignItems:'center',
        color: themes[color].color
    },
    textLabelRight:{
        textAlign:'right',
        color: themes[color].color
    },
    textInput:{
        borderColor:'black',
        marginTop:10,
        height: 50,
        width:'100%',
        backgroundColor: '#eee',
        borderWidth:1,
        borderRadius:25,
        justifyContent: 'center',
        textAlign: "center"
    },
    buttonStyle:{
        borderWidth: 1,
        // borderColor:'rgba(0,0,0,0.2)',
        borderColor: themes[color].ButtonColor,
        alignItems: 'center',
        justifyContent: 'center',
        width: '100%',
        height: 50,
        backgroundColor: themes[color].Background,
        borderRadius: 50,
        // boxShadow: '10px 10px 5px black'
        shadowColor: themes[color].Background, // IOS
        shadowOffset: { height: 5, width: 5 }, // IOS
        shadowOpacity: 5, // IOS
        shadowRadius:25,
        elevation: 8
    },
    ButtonTextStyle:{
        color: themes[color].color
    }
});