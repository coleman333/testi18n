import {StyleSheet} from "react-native";
import * as themes from "../../Constants/Theme";

export const styles = (color) => StyleSheet.create({
    MainContainer: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: themes[color].Background,
        height: "100%"
    },
    MainInnerContainer: {
        flex:8,
        width:'100%',
        justifyContent:'center',
        alignItems:'center'
    },
    TextLabel: {
        color: themes[color].color,
        textAlign:'center',
        alignItems:'center',
    },
    ContainerLabel:{
        marginTop:10,
        // height: 50,
        width:'100%',
        // backgroundColor: '#eee',
        // borderWidth:1,
        // borderRadius:25,
        justifyContent: 'center',
    },
    // textLabel:{
    //     textAlign:'center',
    //     alignItems:'center',
    // },
    textInput:{
        borderColor: themes[color].color,
        marginTop:10,
        height: 50,
        width:'100%',
        backgroundColor: '#eee',
        borderWidth:1,
        borderRadius:25,
        justifyContent: 'center',
        textAlign: "center"
    }
});