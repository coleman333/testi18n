import {StyleSheet} from "react-native";
import * as themes from '../../Constants/Theme';

export const animButtonStyle = (color)=> StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        // elevation: 8
    },
    buttonStyle: {
        color: themes[color].color,
        height: 50,
        width:200,
        borderRadius: 25,
        // backgroundColor: 'white',
        borderWidth:1,
        borderColor: themes[color].ButtonColor,
        justifyContent:'center',
        // elevation: 5,
    },
    buttonText:{
        color: themes[color].color,
        textAlign: 'center',
        // alignItems: 'center',
        fontSize: themes[color].fontSize,
        justifyContent: 'center',
        // textAlignVertical: "center"
    },

    buttonStyle2: {
        height: 50,
        width:200,
        borderRadius: 25,
        // backgroundColor: 'black',
        borderWidth:1,
        justifyContent:'center',
        // elevation: 5
    }
})

export const st = (color) => StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
    },
    label: {
        fontSize: 16,
        fontWeight: 'normal',
        marginBottom: 48,
    },
});

export const Container = (color) => StyleSheet.create({
    container: {
        flex: 1,
        // display: 'flex',
        // width: "100%",
        // height: '100%',
        // backgroundColor: 'white',
        // overflow: 'scroll',
        width: '100%',
        alignItems: 'center',
        // justifyContent: 'space-around'
        justifyContent: 'center'
    },
    upContainer:{
        flex:5,
        justifyContent:'center',
        width: '100%'
    },
    WholeContainerTWFeedback:{
        flex: 1,
        // justifyContent: 'center',
        // alignItems: 'center',
        // backgroundColor: 'gray',
    },
    MainContainer:{
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: themes[color].Background,
        // backgroundColor: 'gray',
    },
    ButtonText:{
        // color: 'white',
        textAlign: 'center',
        // alignItems: 'center',
        // fontSize: 18,
        justifyContent: 'center',
        // textAlignVertical: "center"
    }
    // animStyle:{
    //
    // }
});
