import {NativeModules, Platform} from 'react-native';
import i18n from 'i18next';
// import {reactI18nextModule} from 'react-i18next';
import { initReactI18next } from "react-i18next";

import en from './en';
import ru from "./ru";

const fallbackLocale = 'en';
let locale: string;

switch (Platform.OS) {
    case 'ios': {
        try {
            locale = NativeModules.SettingsManager.settings.AppleLocale;
        } catch (e) {
            console.log('iOS locale error:', e.message);
        }
        break;
    }

    case 'android': {
        try {
            locale = NativeModules.I18nManager.localeIdentifier;
        } catch (e) {
            console.log('Android locale error:', e.message);
        }
        break;
    }

    default:
        locale = fallbackLocale;
}

if (locale) {
    locale = locale.substring(0, 2);
}


const resources = {
    en: en,
    ru: ru,
};

export default i18n.use(initReactI18next).init({
    resources: resources,
    lng: 'en',
    keySeparator: true,
    debug: true,
    fallbackLng: fallbackLocale,
    // ns: Object.keys(en),
    // defaultNS: 'common',
    interpolation: {
        escapeValue: false,
    },
});


/**
 |--------------------------------------------------------------------------------
 |
 | Use translations from `common` locale
 |
 | export const withNamespaces()(ComponentName)
 | ...t('key_in_common_locale')
 |
 |--------------------------------------------------------------------------------
 |
 | Use translations from specific locale
 |
 | export const withNamespaces('locale_name')(ComponentName)
 | ...t('key_in_specified_locale')
 |
 |--------------------------------------------------------------------------------
 |
 | Use translations from several locales
 |
 | export const withNamespaces(['locale_name_1', 'locale_name_2'])(ComponentName)
 | ...t('locale_name:key_in_specified_locale')
 |
 |--------------------------------------------------------------------------------
 */
